// console.log("Hello World");

//What are conditional statements?

//Conditional Statements allow us to control the flow of our program
//It allows us to run a statement/instruction if a condition is met or run another seperate instruction if otherwise

//if, else if, and else statement

let numA = -1;

//if statement
	//executes a statement if a specified condition is true

	if(numA<0){
		console.log("Yes! It is less than zero")
	};

	/*
		Syntax:

		if(condition){
			statement
		}

	*/
	//the result of the expression added in the if's condition must result to true, else, the statement inside if() will not run
	console.log(numA<0);//results to true -- the if statement will run

	numA = 0;

	if(numA<0){
		console.log("Hello")
	};

	console.log(numA<0); //false

	let city = "New York"

	if(city === "New York"){
		console.log("Welcome to New York City!")
	};

	//else if clause

	/*
		-executes a statement if previous conditions are false and if the specified condition is true
		-The "else if" clause is optional and can be added to capture additonal conditions to change the flow of a program

	*/

	let numH = 1;

	if(numA<0){
		console.log("Hello");
	} else if (numH>0){
		console.log("World");
	}

	//we were able to run the else if() statement after we evaluated that the if condtion was failed

	//if the if() condition was passed and run, we will no longer evaluate the else if() and end the process there

	numA = 1;

	if(numA>0){
		console.log("Hello");
	} else if (numH>0){
		console.log("World");
	}

	//else if() statement was no longer run because the if statement was able to run, the evaluation of the whole statement stops there

	city = "Tokyo"

	if(city === "New York"){
		console.log("Welcome to New York City!")
	} else if(city === "Tokyo"){
		console.log("Welcome to Tokyo, Japan!")
	}

	//since we failed the condition for the first if(), we went to the else if() and checked and instead passed that condition

	//else statement

	/*
		-executes a statement if all other conditions are false
		-the "else" statement is optional and can be added to capture any other result to change the flow of a program

	*/

	if(numA<0){
		console.log("Hello");
	} else if (numH === 0) {
		console.log("World");
	} else {
		console.log("Again")
	}

	/*
		Since both the preceeding if and else if conditions are not met/failed, the else statenent was run instead

		Else statements should only be added if there is a preceeding if condition.
		Else statements by itself will not work, howver, if statements will work evn if there is no else statement

	*/

	// else{
	// 	console.log("Will not run without an if")
	// }

	// else if (numH === 0){
	// 	console.log('World')
	// } else {
	// 	console.log('Again')
	// }

	//same goes for an else if, there should be a preceding if() first

	//if, else if, and else statements with functions

	/*
		-most of the times, we would like to use if, else if, and else statements with functions to control the flow of our apppliation

	*/

	let message = "No message.";
	console.log(message);

	function determineTyphoonIntensity(windSpeed){

		if (windSpeed < 30){
			return 'Not a typhoon yet.';
		}else if (windSpeed <= 61) {
			return 'Tropical depression detected.';
		}else if (windSpeed >= 62 && windSpeed <= 88){
			return 'Tropical Storm detected.'
		}else if(windSpeed >= 89 || windSpeed <= 117){
			return 'Severe tropical storm detected.'
		}else{
			return 'Typhoon Detected';
		}

	}
	//returns the string to the variable "message" that invoked it
	message = determineTyphoonIntensity(110);
	console.log(message);

	if (message == "Severe tropical storm detected.") {
		console.warn(message);
	}
	//console.warn() is a good way to print warnings in our console that could help us developers act on certain output with out code

	// Mini Activity #1 Solution

	function oddOrEvenChecker(num){
		if(num % 2 === 0){
			alert(num + " is even!")
		}else{
			alert(num + " is odd!")
		}
	}

	oddOrEvenChecker(54);

	//Mini Activity #2 Solution

	// function ageChecker(num){
	// 	if(num <17){
	// 		alert(num + " is underaged");
	// 		return(false);
	// 	}else{
	// 		alert(num + " is allowed to drink!")
	// 		return true
	// 	}
	// }

	// let isAllowedToDrink = ageChecker(19);
	// console.log(isAllowedToDrink);


	//Truthy and Falsy

	/*
		- In JavaScript a "truthy" value is a value that is considered true when encountered in Boolean context
		-Values in are considered true unless defined otherwise
		-Falsy Values/exceptions for truth:
			1. false
			2. 0
			3. -0
			4. ""
			5. null
			6.undefined
			7. NaN

	*/

	//Truthy Examples
	/*
		-If the result of an expression in a condition results to a truthy value, the condition returns true and the corresponding statements are executed
		-Expressions are any unit of code that can be evaluated to a value
	*/

	if (true){
		console.log('Truthy');
	}

	if (1){
		console.log('Truthy');
	}

	if ([]){
		console.log('Truthy');
	}


	//Falsy Examples

	if (false){
		console.log('Falsy');
	}

	if (0){
		console.log('Falsy');
	}

	if (undefined){
		console.log('Falsy');
	}



	//conditional (ternary) operator - it has a built in return implicit or keyword


	//it takes 3 operand

	//1. condition
	//2. expression to execute if the condition is truthy
	//3. expression to execute if the condition is false

	//single statement execution
		//syntax
		   // (expression) ? ifTrue : ifFalse

	let ternaryResult = (1<18) ? true : false;
	console.log("Result of ternary operator: " + ternaryResult);
	console.log(ternaryResult);

    //multiple statement execution

    let name;

    function isOfLegalAge(){
    	name = "John";
    	return 'You are of the legal age limit';
    }

    function  isunderAge(){
    	name = "Jane";
    	return ' You are under the age limit'
    }

    let age = parseInt(prompt("What is your age?"));
    console.log(age);
    let legalAge = (age>18) ? isOfLegalAge() : isunderAge();
    console.log("Result of ternary operator in function: " + legalAge + ', ' + name)




    //switch statement

    let day = prompt("What day of a week is this today?").toLowerCase();
    console.log(day);

    switch (day) {
    	case 'monday':
    		console.log("The color of the day is red");
    		break;
    	case 'tuesday':
    		console.log("The color of the day is orange");
    		break;
    	case 'wednesday':
    		console.log("The color of the day is yellow");
    		break;
    	case 'thursday':
    		console.log("The color of the day is green");
    		break;
    	case 'friday':
    		console.log("The color of the day is blue");
    		break;
    	case 'saturday':
    		console.log("The color of the day is indigo");
    		break;
    	case 'sunday':
    		console.log("The color of the day is violet");
    		break;
    	default:
    		console.log("Please input a valid day");
    		break;

    }

    function determineBear(bearNumber){
    	let bear;

    	switch (bearNumber) {
    	case 1:
    		alert("Hey, I'm Amy");
    		break;
    	case 2:
    		alert("Hey, I'm Lulu");
    		break;
    	case 3:
    		("Hey, I'm Morgan");
    		break;
    	default:

    		bear = bearNumber + 'is out of bounds!.';
    		break;
   		 }
   		 return bear;
		}
		determineBear(2);



		//try-catch-finally statement - commonly used for error handling

		function showIntensityAlert(windSpeed){
			try {
				alert(determineTyphoonIntensity(windSpeed))
			}catch (error){
				console.log(typeof error);
				console.warn(error.message);
			}finally {
				alert('Intensity updates will show new alert.')
			}
		}
		showIntensityAlert(56);